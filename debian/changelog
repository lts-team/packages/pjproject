pjproject (2.5.5~dfsg-6+deb9u9) stretch-security; urgency=high

  * Non-maintainer upload by the ELTS team.
  * Fix CVE-2023-27585:
    PJSIP is a free and open source multimedia communication library written in
    C. A buffer overflow vulnerability affects applications that use PJSIP DNS
    resolver. It doesn't affect PJSIP users who do not utilise PJSIP DNS
    resolver. This vulnerability is related to CVE-2022-24793. The difference
    is that this issue is in parsing the query record `parse_query()`, while
    the issue in CVE-2022-24793 is in `parse_rr()`. A workaround is to disable
    DNS resolution in PJSIP config (by setting `nameserver_count` to zero) or
    use an external resolver implementation instead.

 -- Markus Koschany <apo@debian.org>  Tue, 18 Apr 2023 21:46:06 +0200

pjproject (2.5.5~dfsg-6+deb9u8) stretch-security; urgency=medium

  * Non-maintainer upload by the LTS Security Team.
  * Backport fixes for
     - CVE-2022-23537 Buffer overread is possible when parsing a specially
       crafted STUN message.
     - CVE-2022-23547 Possible buffer overread when parsing a certain STUN
       message.

 -- Tobias Frost <tobi@debian.org>  Wed, 18 Jan 2023 16:43:30 +0100

pjproject (2.5.5~dfsg-6+deb9u7) stretch-security; urgency=high

  * Non-maintainer upload by the ELTS team.
  * Fix CVE-2022-39244:
    PJSIP is a free and open source multimedia communication library written in
    C. The PJSIP parser, PJMEDIA RTP decoder, and PJMEDIA SDP parser are
    affeced by a buffer overflow vulnerability. Users connecting to untrusted
    clients are at risk.

 -- Markus Koschany <apo@debian.org>  Wed, 26 Oct 2022 14:20:31 +0200

pjproject (2.5.5~dfsg-6+deb9u6) stretch; urgency=high

  * Non-maintainer upload by the Debian ELTS Team.
  * CVE-2022-31031: Prevent a stack buffer overflow vulnerability by setting a
    maximum attribute count.

 -- Chris Lamb <lamby@debian.org>  Fri, 15 Jul 2022 13:03:50 +0100

pjproject (2.5.5~dfsg-6+deb9u5) stretch-security; urgency=medium

  * Non-maintainer upload by the Debian LTS Team.
  * Fix CVE-2022-24763, CVE-2022-24792, CVE-2022-24793

 -- Abhijith PA <abhijith@debian.org>  Sun, 29 May 2022 22:50:20 +0530

pjproject (2.5.5~dfsg-6+deb9u4) stretch-security; urgency=medium

  * Non-maintainer upload by the Debian LTS Team.
  * Regression fix for CVE-2022-23608

 -- Abhijith PA <abhijith@debian.org>  Thu, 31 Mar 2022 13:04:55 +0530

pjproject (2.5.5~dfsg-6+deb9u3) stretch-security; urgency=medium

  * Non-maintainer upload by the Debian LTS Team.
  * Fix CVE-2021-32686, CVE-2021-37706, CVE-2021-41141, CVE-2021-43299,
    CVE-2021-43300, CVE-2021-43301, CVE-2021-43302, CVE-2021-43303,
    CVE-2021-43804, CVE-2021-43845, CVE-2022-21722, CVE-2022-21723,
    CVE-2022-23608, CVE-2022-24754, CVE-2022-24764
  * New symbols pj_strtol2, pj_strtoul3, PJSIP_EINVAL_ERR_EXCEPTION

 -- Abhijith PA <abhijith@debian.org>  Wed, 02 Mar 2022 19:40:34 +0530

pjproject (2.5.5~dfsg-6+deb9u2) stretch-security; urgency=high

  * Non-maintainer upload by the LTS Team.
  * CVE-2021-21375
    Due to bad handling of two consecutive crafted answers to an INVITE,
    the attacker is able to crash the server resulting in a denial of
    service.

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 22 Apr 2021 19:03:02 +0200

pjproject (2.5.5~dfsg-6+deb9u1) stretch-security; urgency=high

  [ Bernhard Schmidt ]
  * Fix various security issues
    - CVE-2017-16872: Overflow when parsing numeric fileds in SIP messages
    - CVE-2017-16875: Double key unregistration in ioqueue component
    - CVE-2018-1000098: Integer overflow in SDP parsing
      (also reported by the Asterisk project as AST-2018-002)
    - CVE-2018-1000099: Null Pointer vulnerability in pjmedia SDP parsing
      (also reported by the Asterisk project as AST-2018-003)
  * Fix resolution of DNS SRV targets that do not have an AAAA record
    (Closes: #881362)

 -- Bernhard Schmidt <berni@debian.org>  Tue, 03 Apr 2018 10:02:31 +0200

pjproject (2.5.5~dfsg-6) unstable; urgency=high

  [ Tzafrir Cohen ]
  * add security patches published by the Asterisk project
    - AST-2017-002: Buffer Overrun in PJSIP transaction layer
      (Closes: #863901)
    - AST-2017-003: Crash in PJSIP multi-part body parser
      (Closes: #863902)

 -- Bernhard Schmidt <berni@debian.org>  Fri, 02 Jun 2017 08:59:42 +0200

pjproject (2.5.5~dfsg-5) unstable; urgency=medium

  [ Bernhard Schmidt ]
  * run dh_clean in clean target
  * Disable debug assertions as recommended for production builds

 -- Bernhard Schmidt <berni@debian.org>  Sun, 18 Dec 2016 22:32:19 +0100

pjproject (2.5.5~dfsg-4) unstable; urgency=medium

  [ Bernhard Schmidt ]
  * Adjust previous changelog to fix lintian error
  * Bump all symbols minimum versions to force tighter dependencies
    (Closes: #847014)
  * Update debian/config_site.h with suggestions from Asterisk git
  * convert package from git-dpm to gbp pq

 -- Bernhard Schmidt <berni@debian.org>  Fri, 16 Dec 2016 14:53:12 +0100

pjproject (2.5.5~dfsg-3) unstable; urgency=medium

  [ Bernhard Schmidt ]
  * Temporarily drop d-devlibdeps generated dependency on libssl-dev
    from libpjproject-dev
  * Add a couple of stability fixes collected by Asterisk
    (third_party/pjproject/patches), all present in pjproject trunk
    - asterisk-0001-r5397-pjsip_generic_array_max_count.patch
    - asterisk-0001-r5400-pjsip_tx_data_dec_ref.patch
    - Fix-1946-Avoid-deinitialization-of-uninitialized-cli.patch
    - asterisk-0002-r5435-add-pjsip_inv_session-ref_cnt.patch
    - asterisk-0003-r5403-pjsip_IPV6_V6ONLY.patch
    - resolver.c-Prevent-SERVFAIL-from-marking-name-server.patch
    - Re-1969-Fix-crash-on-using-an-already-destroyed-SSL.patch
    - r5471-svn-backport-Various-fixes-for-DNS-IPv6.patch
    - r5473-svn-backport-Fix-pending-query.patch
    - r5475-svn-backport-Remove-DNS-cache-entry.patch
    - r5477-svn-backport-Fix-DNS-write-on-freed-memory.patch
   This bunch of patches hopefully fixes #842878

 -- Bernhard Schmidt <berni@debian.org>  Thu, 10 Nov 2016 09:48:05 +0100

pjproject (2.5.5~dfsg-2) unstable; urgency=medium

  * explicitly build-depend on OpenSSL 1.0.2, see #828505

 -- Bernhard Schmidt <berni@debian.org>  Wed, 02 Nov 2016 14:32:51 +0100

pjproject (2.5.5~dfsg-1) unstable; urgency=medium

  * Add myself to uploaders
  * New upstream version 2.5.5~dfsg
    - d/copyright: Exclude third_party/yuv (unclear license)
    - d/copyright: drop third_party/portaudio not present anymore
  * disable libyuv
  * Update symbols file for new version
  * add Build-dep on liboss4-salsa-dev for kfreebsd-any

 -- Bernhard Schmidt <berni@debian.org>  Sun, 30 Oct 2016 20:34:11 +0100

pjproject (2.5.1~dfsg-4) unstable; urgency=medium

  * Update libpjsua2-2v5.symbols for 32-bit platforms
  * disable parallel building causing FTBFS on some architectures

 -- Bernhard Schmidt <berni@debian.org>  Sat, 15 Oct 2016 23:33:59 +0200

pjproject (2.5.1~dfsg-3) unstable; urgency=medium

  * Update debian/libpjsua2-2v5.symbols for GCC-6 (Closes: #831179)

 -- Bernhard Schmidt <berni@debian.org>  Fri, 14 Oct 2016 12:25:18 +0200

pjproject (2.5.1~dfsg-2) unstable; urgency=medium

  * Build-depend on libvo-amrwbenc-dev.
  * Declare compliance with Debian Policy 3.9.8.
  * Stop build-depend on unused libx264-dev.
  * Add d-shlibdev override for libvo-amrwbenc.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 07 Jun 2016 22:46:25 +0200

pjproject (2.5.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s).

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Normalize and sort copyright holders.
    + Fix use double-space as copyright-holder delimiter.
    + Extend coverage for main author to include recent years.
    + Use License-Grant and License-Reference fields for GPL license.
      Thanks to Ben Finney.
    + Merge Files sections with identical license-grant.
    + Generalize comment about additional ignored licenses.
    + Fix add additional GPL-granting copyright holders.
    + Use separate Files section for each public-domain (non-)grant.
    + Use License-Grant and License-Reference fields for Apache license.
    + Strip convenience code copies gsm portaudio resample speex srtp
      when repackaging upstream source.
    + Stop track files or licenses of non-redistributed convenience code
      copies.
  * Add copyright-check script.
  * Update patches to not mess with autogenerated files.
  * Bump debhelper compatibility level to 10.
    Adjust rules to use dh_autoreconf.
    Tighten build-dependency on debhelper.
  * Drop import script (gbp import-orig clashes with git-dpm patches).
    Document in README.source how to import new upstream source.
  * Unfuzz patch, and drop ones resolved upstream.
  * Build-depend on libopus-dev libwebrtc-audio-processing-dev.
  * Simplify d-shlibdeps call and instead tighten build-dependency on
    d-shlibs.
  * Update symbols files.
  * Add d-shlibdev override for libopus.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 07 Jun 2016 18:59:26 +0200

pjproject (2.4.5~dfsg-4) unstable; urgency=medium

  * Fix avoid build-depending on libv4l-dev on the Hurd:
    + Adapt symbols.
    + Resolve libpjproject-dev dependencies related to linked libraries
      during build.
    + Build-depend on d-shlibs.
  * Modernize Vcs-* field URLs:
    + Use https protocol.
    + Use cgit viewer.
  * Build-depend unversioned on debhelper: Needed version satisfied even
    in oldstable.
  * Add lintian override regarding debhelper 9.
  * Declare compliance with Debian Policy 3.9.7.
  * Update symbols file (affects only optional symbols).
  * Fix stop have libpjproject-dev depend on libx264-dev: Seems unused.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 01 Apr 2016 15:03:42 +0200

pjproject (2.4.5~dfsg-3) unstable; urgency=medium

  * Update symbois.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Mar 2016 19:22:31 +0100

pjproject (2.4.5~dfsg-2) unstable; urgency=medium

  * Move packaging to Debian VoIP team (with Tzafrir's consent).
    Add myself as uploader.
  * Use pkgkde-symbolshelper. Build-depend on pkg-kde-tools.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Mar 2016 17:09:28 +0100

pjproject (2.4.5~dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ upstream ]
  * New release(s).
    + Use the OpenSSL RNG if available, instead of via SRTP.
      Closes: Bug#793972.
    + Support compilation with OpenSSL without OpenSSLv3 support.
      Closes: Bug#805082. Thanks to Peter Green.

  [ Tzafrir Cohen ]
  * Fix add Files section for bar-descriptor.xml to debian/copyright.
    Closes: #793051. Thanks to Thorsten Alteholz.
  * Move arch-dependent headers to a triplet subdir.
    Closes: #793097. Thanks to Jakub Wilk.
  * Bump soname for libpjsua2: Add C++ transitional v5 suffix.
    Closes: Bug#793094. Thanks to Jakub Wilk and Simon McVittie.
  * Replace deprecated FFmpeg API.
    Closes: Bug#803855, #801535. Thanks to Andreas Cadhalpun.
  * Copy -dev build deps to deps of -dev package.
    Closes: #795825, #801535. Thanks to Julien Cristau, Simon McVittie
    and Andreas Beckmann.

  [ Jeremy Lainé ]
  * Reduce library linkage.
    Closes: Bug#793141. Thanks to James Cloos.

  [ Jonas Smedegaard ]
  * Fix typo in long descriptions.
  * Build-depend on virtual libsrtp-dev (not libsrtp0-dev) to ease
    pending transition.
  * Fix have libpjproject-dev depend on libpjsua2-2v5 (not libpjsua2-2).
  * Fix add version mangling hint to watch file.
  * Fix strip debian revision from symbols files.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Mar 2016 11:42:57 +0100

pjproject (2.4~dfsg-1) unstable; urgency=low

  * New upstream release: now an upstream version (Closes: #725326).
    - Update symbols for version 2.3, 2.4
    - We use upstream source. No need for the README
    - rules: config_site moved
    - CODEC_ID_* changed to AV_CODEC_ID_*
    - Extra library: pjsua2 (libpjsua2-2. Package libpjsua2 is pjsua).
  * -dev package: depend explicitly on libsrtp0-dev due to -lsrtp in
    the pkg-config file.
  * rules: run 'make dep' at build time.
  * rules: 'build' not cnfused by directory 'build'
  * Enable opencore-amr codec support
  * Do enable IPv6 support in config_site (Closes: #787964).
  * README.source: we use git-dpm
  * Do enable video
  * compat level 9
  * multi-arch support
    - control: packages pre-depend on multi-arch
  * Remove offending build flags from pkgconfig file (lintian).
  * A disabled tests target
  * extra files to remove after tests
  * Reduce number of libraries linked with pjlib-util
  * copyright: Fix syntax (gsm)
  * Remove video libs dependency for pj-utils and pjnath
  * package python-pjproject (Closes: #768578)
    - Depend on python-dev
  * Standards version 3.9.6 (no change needed)
  * copyright: Don't use '[]' wildcards

 -- Tzafrir Cohen <tzafrir@debian.org>  Sun, 28 Jun 2015 23:53:29 +0300

pjproject (2.1.0.0.ast20130823-1) unstable; urgency=low

  [ Jeremy Lainé ]
  * Fix dh_auto_clean on an unconfigured source tree (Closes: #722013).
  * Fix syntax error in machine-readable debian/copyright.
  * Update Standards-Version to 3.9.4 (no changes).

  [ Tzafrir Cohen ]
  * Further copyright fixes.
  * New upstream git snapshot:
    pkgconfig_nodestdir.patch, soname.patch dropped: merged upstream.
  * fix_gcc_warn.patch: remove some build warnings.

 -- Tzafrir Cohen <tzafrir@debian.org>  Thu, 12 Sep 2013 16:53:51 +0300

pjproject (2.1.0~ast20130801-1) unstable; urgency=low

  * Initial release. (Closes: #708122)

 -- Tzafrir Cohen <tzafrir@debian.org>  Wed, 07 Aug 2013 22:20:07 +0300
